/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cion <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/05 17:46:22 by cion              #+#    #+#             */
/*   Updated: 2018/02/05 17:46:24 by cion             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Writes the string pointed by format to the standard output (stdout),
** replacing any format specifier in the same way as printf does, but using the
** elements in the variable argument list identified by arg instead of
** additional function arguments.
*/
int		ft_printf(const char *format, ...)
{
	int		i;
	int		len;
	va_list	ap;

	len = 0;
	va_start(ap, format);
	while (*format)
	{
		if (*format == '%')
		{
			format++;
			if ((i = to_output(&format, ap)) == -1)
				break ;
			len += i;
		}
		else
		{
			ft_putchar(*format);
			len++;
		}
		format++;
	}
	va_end(ap);
	return (len);
}





/*int		ft_printf(const char *format, ...)
{
	va_list		ap;
	int			i;
	int			p;

	i = -1;
	p = 0;
	va_start(ap, format);
	while (format[++i] != '\0')
	{
		if (format[i] == '%')
			parse_all(&i, format, ap, &p);
		else
			ft_putchar(format[i]);
	}
	//i = find_format((char**)&format, ap);
	va_end(ap);
	return (p);
}*/


/*int		ft_format(char *format)
{
	char	*modifiers[] = { "hh", "h", "l", "ll", "j", "z" };
	char	*flags[] = { "#", "0", "-", "+" };
	char	*conversions[] = { "s", "S", "p", "d", "D", "i", "o", \
						"O", "u", "U", "x", "X", "c", "C" };

char	*modifiers[] = 
	{
		[0] = "hh";
		[1] = "h";
		[2] = "l";
		[3] = "ll";
		[4] = "j";
		[5] = "z";
	};
	char	*flags[] = 
	{
		[0] = "#";
		[1] = "0";
		[2] = "-";
		[3] = "+";
	};
	char	*conversions = 
	{
		[0] = "s";
		[1] = "S";
		[2] = "p";
		[3] = "d";
		[4] = "D";
		[5] = "i";
		[6] = "o";
		[7] = "O";
		[8] = "u";
		[9] = "U";
		[10] = "x";
		[11] = "X";
		[12] = "c";
		[13] = "C";
	};
	
	int		i;
	char	*flags;
	char	*modifiers;
	char	*conversions;

	modifiers = "hh,h,l,ll,j,z";
	flags = "#0-+";
	conversions = "sSpdDioOuUxXcC";
	i = 0;

	while (ft_strchr(flags, format[i]) != 0)
		i++;
	while (ft_strchr(modifiers, format[i]) != 0)
		i++;
	while (ft_isdigit(format[i]))
		i++;
	if (format[i] == '.')
		i++;
	while (ft_strchr(conversions, format[i]) != 0)
		return (++i);
	return (0);

}

void	start_validation(char *format)
{
	int i;
	while (*format)
	{
		if (*format != '%')
			format++;
		else
		{
			format++;
			if ((i = ft_format(format) == 0))
				exit(0);
			format += i;
		}
	}
}



#include <stdio.h>
int main(void)
{

	printf("%d\n", ft_format("D"));
	return (0);
}

int		found_modifiers(char c)
{
	int i;

	i = 0;
	while (modifiers[i])
	{
		if (modifiers[i] == c)
			return (1);
		i++;
	}
	return (0);
}

int		found_flag(char c)
{
	int i;

	i = 0;
	while (flags[i])
	{
		if (flags[i] == s)
			return (1);
		i++;
	}
	return (0);
}

int		found_conversions(char c)
{
	int i;

	i = 0;
	while (conversions[i])
	{
		if (conversions[i] == c)
			return (1);
		i++;
	}
	return (0);
}


int		ft_printf(const char *format, ...)
{
	va_list	list;
	char	*l;

	va_start(list, format);
	l = format;
	start(list, l);
	va_end(list);
}*/
