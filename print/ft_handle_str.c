/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_handle_str.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cion <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/05 17:45:58 by cion              #+#    #+#             */
/*   Updated: 2018/02/05 17:46:00 by cion             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Prints string.
*/
int		ft_str(va_list arg, t_intel *intel)
{
	int		i;
	char	*dupl;
	char	*str;

	if (!(str = va_arg(arg, char*)))
		str = ft_strdup("(null)");
	if (intel->flags & PREC)
	{
		if (!(dupl = (char *)malloc(sizeof(char) * intel->precision)))
			return (0);
		dupl = ft_strncpy(dupl, str, intel->precision);
		str = dupl;
	}
	i = ft_strlen(str);
	if (!(intel->flags & MINUS))
		i = ft_print_space(intel->width, i, intel->flags);
	ft_putstr(str);
	if (intel->flags & MINUS)
		i = ft_print_space(intel->width, i, intel->flags);
	return (i);
}

/*
** Prints char.
*/
int		ft_chr(va_list ap, t_intel *intel)
{
	int		i;
	char	c;

	c = va_arg(ap, int);
	i = sizeof(char);
	if (!(intel->flags & MINUS))
		i = ft_print_space(intel->width, i, intel->flags);
	ft_putchar(c);
	if (intel->flags & MINUS)
		i = ft_print_space(intel->width, i, intel->flags);
	return (i);
}
