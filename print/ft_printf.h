/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cion <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/05 17:46:28 by cion              #+#    #+#             */
/*   Updated: 2018/02/05 17:46:31 by cion             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "libft/includes/libft.h"
# include <stdarg.h>
# include <wchar.h>
# include <stdint.h>

/*
** ft_printf prototype
*/
int		ft_printf(const char *format, ...);

typedef struct s_intel
{
	int		width;
	int		precision;
	int		flags;
}				t_intel;

# define SPECS		"sSpdDioOuUxXcC"
# define SPECSCOUNT	(ft_strlen(SPECS))

/*
** Encoding for the specifiers
*/
# define STRING 	(0)
# define WSTRING 	(1)
# define POINTER	(2)
# define LOWD		(3)
# define UPPD		(4)
# define LOWI		(5)
# define LOWO		(6)
# define UPPO 		(7)
# define LOWU 		(8)
# define UPPU 		(9)
# define LOWX 		(10)
# define UPPX 		(11)
# define LOWC 		(12)
# define UPPC 		(13)

/*
** Macro to shift x 1 bit to the left
*/
# define FBIT(x)	(1 << (x))

/*
** Encoding every single attribute of printf, 
** representing one bit for every single one of them.
*/
# define STRING_B 	(FBIT(0))
# define WSTRING_B	(FBIT(1))
# define POINTER_B 		(FBIT(2))
# define LOWD_B 		(FBIT(3))
# define UPPD_B	(FBIT(4))
# define LOWI_B	(FBIT(5))
# define LOWO_B	(FBIT(6))
# define UPPO_B	(FBIT(7))
# define LOWU_B	(FBIT(8))
# define UPPU_B	(FBIT(9))
# define LOWX_B	(FBIT(10))
# define UPPX_B	(FBIT(11))
# define LOWC_B	(FBIT(12))
# define UPPC_B	(FBIT(13))

# define HASH	(FBIT(14))
# define ZERO	(FBIT(15))
# define PLUS	(FBIT(16))
# define MINUS	(FBIT(17))
# define SPACE	(FBIT(18))

# define HH	(FBIT(19))
# define H	(FBIT(20))
# define L	(FBIT(21))
# define LL	(FBIT(22))
# define J	(FBIT(23))
# define Z	(FBIT(24))

# define WIDTH	(FBIT(25))
# define PREC	(FBIT(26))


/*
** ft_handle_str.c
*/
int 	ft_str(va_list arg, t_intel *intel);
int 	ft_chr(va_list ap, t_intel *intel);

/*
** ft_output.c
*/
int 	print_all(char format, va_list ap, t_intel *intel, int i);
int 	to_output(const char *frm[], va_list ap);
int 	ft_print_space(int w, int i, int intel);
int 	ft_print_nothing(char c, t_intel *intel);

/*
** ft_specif.c
*/
int 	specpos(char c, char *specifier);
int		findspec(char c, t_intel *intel, char *specifier);
int 	parsespec(va_list ap, char **frm, t_intel *intel);

/*
** ft_flags.c
*/
int 	flags(char c, t_intel *intel);
int 	width(va_list ap, char *frm, t_intel *intel);
int 	precision(va_list arg, char *frm, t_intel *intel);
int 	get_all_fl(char **frm, va_list ap, t_intel *intel);


#endif
