/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_output.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cion <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/05 17:46:12 by cion              #+#    #+#             */
/*   Updated: 2018/02/05 17:46:16 by cion             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Prints number of spaces.
*/
int		ft_print_space(int w, int i, int intel)
{
	while ((w - i) > 0)
	{
		ft_putchar(((intel & ZERO) && !(intel & MINUS)) ? '0' : ' ');
		i++;
	}
	return (i);
}

/*
** If no specifier is found, prints the character, plus the specific
** justification given the minus flag: (left or right).
*/
int		ft_print_nothing(char c, t_intel *intel)
{
	int 	i;

	if (!(intel->flags & MINUS))
		i = ft_print_space(intel->width, sizeof(char), intel->flags);
	ft_putchar(c);
	if (intel->flags & MINUS)
		i = ft_print_space(intel->width, sizeof(char), intel->flags);
	return (i);
}

/*
** Prints the specific specifier given from the format.
*/
int		print_all(char frm, va_list ap, t_intel *intel, int i)
{
	int 	output;

	if (i == STRING)
		output = ft_str(ap, intel);
	else if (i == LOWC)
		output = ft_chr(ap, intel);
	else
		ft_print_nothing(frm, intel);
	return (output);
}

/*
** If successful, the total number of characters written is returned.
** On failure, a negative number is returned.
*/
int		to_output(const char *frm[], va_list ap)
{
	int 	specifier;
	t_intel	intel;

	intel.width = 0;
	intel.precision = 0;
	intel.flags = 0;
	specifier = parsespec(ap, (char**)frm, &intel);
	if (!**frm)
		return (-1);
	return (print_all((char**)frm, ap, &intel, specifier));
}
