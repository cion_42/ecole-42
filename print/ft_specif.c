/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_specif.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cion <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/05 17:46:40 by cion              #+#    #+#             */
/*   Updated: 2018/02/05 17:46:42 by cion             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Gets every specifier position from the format.
*/
int 	specpos(char c, char *specifier)
{
	int 	position;

	position = 0;
	while (specifier[position])
	{
		if (c == specifier[position])
			return (position);
		position++;
	}
	return (-1);
}

/*
** Gets which specifier is in the format, and encode it given its respective bit
** value (going from bit 0, to bit 13 for the mandatory
** specifiers, and bit twenty-seven (27) for the bonus, the binary).
*/
int		findspec(char c, t_intel *intel, char *specifier)
{
	int 	*array;
	int 	spec;

	array = (int *)malloc(sizeof(int) * SPECSCOUNT);
	array[STRING] = STRING_B;
	array[WSTRING] = WSTRING_B;
	array[POINTER] = POINTER_B;
	array[LOWD] = LOWD_B;
	array[UPPD] = UPPD_B;
	array[LOWI] = LOWI_B;
	array[LOWO] = LOWO_B;
	array[UPPO] = UPPO_B;
	array[LOWU] = LOWU_B;
	array[UPPU] = UPPU_B;
	array[LOWX] = LOWX_B;
	array[UPPX] = UPPX_B;
	array[LOWC] = LOWC_B;
	array[UPPC] = UPPC_B;
	if (((spec = specpos(c, specifier)) != -1) && c)
		intel->flags |= array[spec];
	free(array);
	return (spec);
}

/*
** Parse the specifier, returning the specifier positing.
** If the attribute isn't valid, returns a printf error.
*/
int 	parsespec(va_list ap, char **frm, t_intel *intel)
{
	int 	spec;
	int 	ok;
	char 	*converse;

	intel->flags = 0;
	if (!(converse = (char *)malloc(sizeof(char) * SPECSCOUNT)))
		return (0);
	converse = ft_strncpy(converse, SPECS, SPECSCOUNT);
	spec = -1;
	while (**frm)
	{
		if ((specpos(**frm, converse) != -1) && \
			(spec = findspec(**frm, intel, converse)))
			break ;
		if (!(ok = get_all_fl(frm, ap, intel)))
			break ;
	}
	free(converse);
	return (spec);
}
